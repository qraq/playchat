from django.contrib import admin

from .models import Post, Tag, get_add_tags

class postAdmin(admin.ModelAdmin):
	list_display = ('__str__', 'user', 'parent', 'active')
	search_fields = ('content',)
	list_filter = ('active',)
	ordering = ('-id',)
	list_per_page = 50
	readonly_fields = ('tags','date')

	def save_related(self, request, form, formsets, change):
		#to cos powoduje ze tagi zadzialaja nam w panelu admina
		super(postAdmin, self).save_related(request, form, formsets, change)
		get_add_tags(form.instance)

admin.site.register(Post, postAdmin)

class tagAdmin(admin.ModelAdmin):
	list_display = ('__str__', 'date', 'active')
	search_fields = ('name',)
	ordering = ('-date',)
	list_filter = ('date', 'active',)
	list_per_page = 50
	readonly_fields = ('date',)

admin.site.register(Tag, tagAdmin)