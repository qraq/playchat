# -*- coding: utf-8 -*-

import re

from django.db import models
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.utils.timezone import now, localtime

from mptt.models import MPTTModel, TreeForeignKey

POST_ACTIVE = (
    (0, 'Aktywny'),
    (1, 'Nieaktywny'),
    (2, 'Usunięty')
    )

def hashtag_validator(value):
    if not re.match(r'^\w{2,100}$', value):
        raise ValidationError('Dozwolone znaki: A-Z, a-z, 0-9, _')

def get_add_tags(object):
    '''
    Znajdujemy tagi w tekscie, ładujemy lub dodajemy do bazy  
    '''
    object.tags.clear()
    used_tags = re.compile(r'(?:^|\s)#(\w{2,100})', re.MULTILINE).findall(object.content)
    for t in used_tags:
        tag, created = Tag.objects.get_or_create(name=t)
        object.tags.add(tag)

class HashtagField(models.CharField):
    description = 'Tag do 100 znaków, A-Z,a-z,0-9,_.'

    def __init__(self, *args, **kwargs):
        kwargs['max_length'] = 100
        kwargs['validators'] = [hashtag_validator]
        super(HashtagField, self).__init__(*args, **kwargs)

    def get_prep_value(self, value):
        return value.lower()

######MODELS######

class Tag(models.Model):
    name = HashtagField(verbose_name='Nazwa taga', unique=True)
    date = models.DateTimeField(verbose_name='Data dodania', auto_now_add=True, 
                                editable=True, help_text='Data pierwszego użycia')
    active = models.BooleanField(verbose_name='Aktywny', default=True)

    class Meta:
        verbose_name = 'Hashtag'
        verbose_name_plural = 'Hashtagi'
    
    def __str__(self):
        return '#{}'.format(self.name)

class Post(MPTTModel):
    user = models.ForeignKey(User, verbose_name='Autor postu')
    content = models.TextField(verbose_name='Treść', max_length=4000)
    parent = TreeForeignKey('self', null=True, blank=True, default=None, 
                            related_name='Odpowiedz', verbose_name='Odpowiedź do')
    date = models.DateTimeField(verbose_name='Data dodania', auto_now_add=True)
    active = models.IntegerField(choices=POST_ACTIVE, verbose_name='Aktywność', default=0)
    tags = models.ManyToManyField(Tag, blank=True, editable=False, verbose_name='Tagi')

    class Meta:
        verbose_name = 'Post'
        verbose_name_plural = 'Posty'

    def __str__(self):
        return '{}...'.format(self.content[:40])

    def save(self):
        super(Post, self).save()
        get_add_tags(self)